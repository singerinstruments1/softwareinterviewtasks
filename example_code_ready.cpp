// This code controls a simple test rig
// Motors are controlled using the TeensyStep library which is included at the top.

#include <Arduino.h>
#include "TeensyStep.h"


// Pinout
int stepdir = 16;
int stepclk = 17;
int stepen = 18;
int halleffect1 = 15;
int light1 = 5;  //White
int light2 = 20; //uv
int light3 = 21; //blue
int light4 = 23; //Base White
int light5 = 22; //Cyan

// Stepper Motor Control
Stepper motor(stepclk, stepdir);
StepControl controller;
long NORMSPEED = 35000; // Speed of motor in normal operation
long NORMACCEL = 60000; // Acceleration of motor
int HOMINGSPEED = 15000; // Speed of motor when homing
int HOMINGACCEL = 10000;

bool Homed = false; // Motor homed status
bool Homing = false;
bool Moving = false;

#define MOT_ENABLED 0
#define MOT_NOTENABLED 1

void start()
{
  // Setup Serial
  Serial.begin(115200); Serial.setTimeout(1);
  
  // Set pin moves
  pinMode(halleffect1,INPUT); //set up homing as input
  pinMode(stepen,OUTPUT);
  pinMode(light1,OUTPUT); pinMode(light2,OUTPUT); 
  pinMode(light3,OUTPUT); pinMode(light4,OUTPUT);
  pinMode(light5,OUTPUT); 

  // Setup stepper motor
  motor.setMaxSpeed(NORMSPEED);
  motor.setAcceleration(NORMACCEL);
  motor.setPosition(0);
  digitalWrite(stepen,MOT_NOTENABLED);
  
  Serial.println("Ready!");
  homeMotor();
}

void loop()
{
	if (digitalRead(halleffect1))
	{
		if (Homing)
		{
			controller.emergencyStop();
			motor.setPosition(0);
			Homed = true;
			Homing = false;
			motor.setMaxSpeed(NORMSPEED); 
			motor.setAcceleration(NORMACCEL);
			Serial.println("TRH_OK");
		}
	}
	
	if (Serial.available() > 0)
	{
		int ticket = 99; char TestReq[64]; char Income[64];
		String Message = Serial.readString(); 
		Message.trim();
		Message.toCharArray(Income,64);
		
		int select = (int)Income[2] - 48; if (select < 0) {select = 99;} 
		String val; val += Income[4]; val += Income[5]; val += Income[6]; val += Income[7]; val += Income[8]; val += Income[9]; int value = val.toInt();  
		
		if (select == 'm') { // Move motor
			motor.setTargetAbs(value);
			controller.moveAsync(motor);
			Moving = true;
			while (controller.isRunning())
			{
			}
			Moving = false;
		}
	}
}

void homeMotor() {
  Serial.println("TRH");
  
  Homed = false;
  Homing = true;
  
  controller.emergencyStop(); 
  motor.setMaxSpeed(HOMINGSPEED); motor.setAcceleration(HOMINGACCEL); motor.setPosition(0);
  
  digitalWrite(stepen,MOT_ENABLED);
  motor.setTargetAbs(-20000);
  controller.moveAsync(motor);
}